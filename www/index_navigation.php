<?php
	echo '<div id="navigation">';
	$file = file_get_contents('question_set.json');
	$json = json_decode($file, true);

	if(isset($_SESSION["CurrentQuestion"]))
	{
		if($_SESSION["CurrentQuestion"] == 0) 
		{
			echo '<input type="submit" class="btn btn-xl btn-default animated infinite pulse" value="Begin" />';
		}
		else if($_SESSION["CurrentQuestion"] == sizeof($json) - 1)
		{
			echo '<button type="button" onClick="goBack();" class="btn btn-xl btn-default back">Back</button>';
			echo '<input type="submit" class="btn btn-xl btn-default animated infinite pulse" value="Finish" />';
		}
		else
		{
			echo '<button type="button" onClick="goBack();" class="btn btn-xl btn-default back">Back</button>';
			echo '<input type="submit" class="btn btn-xl btn-default" value="Next" />';
		}
	}
	echo '</div>';
?>
