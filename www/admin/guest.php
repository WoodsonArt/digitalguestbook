<?php
	$JsonFileCache = file_get_contents('../question_set.json');
	$JsonFileParse = json_decode($JsonFileCache, true);

	$data_results = file_get_contents('../results.json');
	$tempArray = json_decode($data_results, true);
	$entry = array();
	for ($i = 0; $i < count($tempArray); $i++)
	{
		$data = $tempArray;
		if($data[$i]["uniq"] == $_GET["uuid"])
		{
			$entry = $data[$i];
		}
	}
	$dataDate = date('m-d-Y', strtotime($entry["date"]));
?>


<!DOCTYPE html>
<html>
	<head>
        <?php include("include/header.php"); ?>
	</head>
	<body>
		<br />
		<div class="container">
            <?php include("include/navigation.php"); ?>


			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">Guest Metadata</div>
						<ul class="list-group">
							<?php
								echo '<li class="list-group-item">Guest ID: '.$entry["uniq"].'</li>';
								echo '<li class="list-group-item">Survey Completed: '.$dataDate.'</li>';
							?>
						</ul>
					</div>
				</div>
				<div class="col-sm-8">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Question</th>
								<th>Answer</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach(array_keys($entry) as $paramName)
								{
									if($paramName == "uniq" || $paramName == "date")
									{
										continue;
									}
									for($i = 0; $i < count($JsonFileParse); $i++)
									{
										$Question = $JsonFileParse[$i];
										if($Question["ID"] == $paramName)
										{
											echo '<tr>';
											echo '<td>'.$Question["QuestionTitle"].'</td>';
											if($Question["QuestionType"] == "Radio")
											{
												echo '<td>'.$Question["EntryTypeOptions"][$entry[$paramName]].' </td>';
											}
											else
											{
												echo '<td>'.$entry[$paramName].' </td>';
											}
											echo '</tr>';
										}
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
	</body>
</html>
