<?php
	$JsonFileCache = file_get_contents('../question_set.json');
	$JsonFileParse = json_decode($JsonFileCache, true);
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include("include/header.php"); ?>
	</head>
	<body>
        <br />
		<div class="container">
            <?php include("include/navigation.php"); ?>
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default">
				        <div class="panel-heading">Recent Guests</div>
                        <ul class="list-group">
                            <?php
                                $data_results = file_get_contents('../results.json');
                                $tempArray = json_decode($data_results, true);
                                for ($i = 0; $i < 5; $i++)
                                {
                                    $data = array_slice($tempArray, -5);
                                    $dataDate = date('m/d/Y', strtotime($data[$i]["date"]));
                                    echo '<li class="list-group-item"><a href="guest.php?uuid='.$tempArray[$i]["uniq"].'">'.$tempArray[$i]["uniq"].'</a></li>';
                                }
                            ?>
                        </ul>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="panel panel-default">
						<div class="panel-heading">Generate Report</div>
						<div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>Select Date Range</h4>
                                    <hr />
                                    <label for="meeting">&nbsp;Report Start</label><br /><input id="start" type="date" value="<?php echo date('Y-m-d')?>"/><br /><br />
                                    <label for="meeting">&nbsp;Report End</label><br /><input id="end" type="date" value="<?php echo date('Y-m-d')?>"/>
                                </div>
                                <div class="col-md-8">
                                    <h4>Select Survey Data</h4>
                                    <hr />
                                    <?php
                                        for($i = 1; $i < count($JsonFileParse) - 1; $i++)
                                        {
                                            if($JsonFileParse[$i]["QuestionType"] == "Radio")
                                            {
                                                echo '<span>'.$JsonFileParse[$i]["QuestionTitle"].'</span><br />';
                                                for($j = 0; $j < count($JsonFileParse[$i]["EntryTypeOptions"]); $j++)
                                                {
                                                    echo '<input type="checkbox" name="question">&nbsp;'.$JsonFileParse[$i]["EntryTypeOptions"][$j].'</input><br />';
                                                }
                                                echo '<br />';
                                            }
                                            else
                                            {
                                                echo '<input type="checkbox" name="question">&nbsp;'.$JsonFileParse[$i]["QuestionTitle"].'</input><br />';
                                                if($JsonFileParse[$i + 1]["QuestionType"] == "Radio")
                                                {
                                                    echo '<br />';
                                                }
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-6"><a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-download-alt"></i><span>&nbsp;Excel</span></a></div>
								<div class="col-md-6"><a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-download-alt"></i><span>&nbsp;CSV / Raw Data</span></a></div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
