<?php
	$JsonFileCache = file_get_contents('../question_set.json');
	$JsonFileParse = json_decode($JsonFileCache, true);
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include("include/header.php"); ?>
	</head>
	<body>
		<br />
		<div class="container">
            <?php include("include/navigation.php"); ?>
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">Question Statistics</div>
						<ul class="list-group">
							<?php
								echo '<li class="list-group-item">Number of Questions: '.count($JsonFileParse).'</li>';
							?>
						</ul>
                        <div class="panel-body row">
                            <div class="col-md-6"><a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i><span> Add Question</span></a></div>
				            <div class="col-md-6"><a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-danger btn-block"><i class="glyphicon glyphicon-minus"></i><span> Remove Question</span></a></div>
                        </div>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="panel panel-default">
						<div class="panel-heading">Questions</div>
						<ul class="list-group">
							<?php
								for($i = 0; $i < count($JsonFileParse); $i++)
								{
                                    $title = str_replace("<br />", ", ", $JsonFileParse[$i]["QuestionTitle"]);
									echo '<li class="list-group-item"><a href="editquestion.php?id=">'.($i+1).'. '.$title.'</a></li>';
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
