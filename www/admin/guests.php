<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
	<body>
		<br />
		<div class="container">
			<?php include("include/navigation.php"); ?>
			<br />
			<div class="row">
                <div class="col-md-4">
                    <div class="panel panel-default">
						<div class="panel-heading">Question Statistics</div>
                        <div class="panel-body row">
                            <div class="col-md-6"><a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i><span> Add Response</span></a></div>
				            <div class="col-md-6"><a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-danger btn-block"><i class="glyphicon glyphicon-minus"></i><span> Remove Data</span></a></div> 
                        </div>
					</div>
                </div>
                <div class="col-md-8">
                    <?php
                        $data_results = file_get_contents('../results.json');
                        $tempArray = json_decode($data_results, true);
                        for ($i = 0; $i < count($tempArray); $i++) 
                        {
                            $data = $tempArray;
                            $dataDate = date('m-d-Y', strtotime($data[$i]["date"]));
                            echo '<div class="col-md-6"><a href="guest.php?uuid='.$tempArray[$i]["uniq"].'">('.$dataDate.') Guest Entry #'.$tempArray[$i]["uniq"].'</a></div>';
                        }			
                    ?>
                </div>
			</div>
		</div>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
	</body>
</html>