<?php
	session_start();

    function file_get_contents_curl($url) 
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

	$SessionCurPtr = "CurrentQuestion";
	$ResultsLatPtr = "StoredLat";
	$ResultsLngPtr = "StoredLng";
	$ResultsLocPtr = "StoredLoc";
	$SessionPrvPtr = "NavStacks";
	$LocalZipCodes = ["54401", "54402", "54403", "54476", "54452"];
	$RequestMethod = $_SERVER['REQUEST_METHOD'] == "POST";
	$JsonFileCache = file_get_contents('question_set.json');
	$JsonFileParse = json_decode($JsonFileCache, true);

	if(isset($_SESSION[$SessionCurPtr]))
	{
		$SessionCurrID = $_SESSION[$SessionCurPtr];
		if($RequestMethod)
		{
			if(isset($_POST["back"]))
			{
				array_pop($_SESSION[$SessionPrvPtr]);
				$_SESSION[$SessionCurPtr] = end($_SESSION[$SessionPrvPtr]);
			}
			else
			{
				$PreviousQuestion = $JsonFileParse[$SessionCurrID];
				$PreviousQuestionEntryType = $PreviousQuestion["QuestionType"];
				switch ($PreviousQuestionEntryType)
				{
					case "ZipCode":
						$ZipCode = $_POST["input-zipcode"];
						$ZipCodeCheck = (int) in_array($ZipCode, $LocalZipCodes);
						$ZipCodePaths = $PreviousQuestion["EntryTypeOptionsPathing"];
						$PrepStr = str_replace(' ', '+', $ZipCode);
						$Geocode = file_get_contents_curl('http://maps.google.com/maps/api/geocode/json?address='.$PrepStr.'&sensor=false');
						$Results = json_decode($Geocode);
						$_SESSION[$SessionCurPtr] = $ZipCodePaths[$ZipCodeCheck];
						$_SESSION[$ResultsLatPtr] = $Results->results[0]->geometry->location->lat;
						$_SESSION[$ResultsLngPtr] = $Results->results[0]->geometry->location->lng;
                        
                        $Locality = "Unknown Locality";
                        $Administrative_Area_Level_1 = "Unknown State";
                        for($ix = 0; $ix < sizeof($Results->results[0]->address_components); $ix++)
                        {
                            if ($Results->results[0]->address_components[$ix]->types[0] == "locality")
                            {
                                $Locality = $Results->results[0]->address_components[$ix]->long_name;
                            }
                        }
                        for($jx = 0; $jx < sizeof($Results->results[0]->address_components); $jx++)
                        {
                            if ($Results->results[0]->address_components[$jx]->types[0] == "administrative_area_level_1")
                            {
                                $Administrative_Area_Level_1 = $Results->results[0]->address_components[$jx]->short_name;
                            }
                        }
                        $_SESSION[$ResultsLocPtr] = $Locality.", ".$Administrative_Area_Level_1;
						
						array_push($_SESSION[$SessionPrvPtr], $ZipCodePaths[$ZipCodeCheck]);
						$_SESSION["Results"][(int)$PreviousQuestion["ID"]] = $ZipCode;
						break;
					case "Paragraph":
						$NextQuestion = $PreviousQuestion["NextQuestion"];
						$_SESSION["Results"][(int)$PreviousQuestion["ID"]] = $_POST["input-paragraph"];
						$_SESSION[$SessionCurPtr] = $NextQuestion;
						array_push($_SESSION[$SessionPrvPtr], $NextQuestion);
						break;
					case "Slider":
						$NextQuestion = $PreviousQuestion["NextQuestion"];
						$_SESSION["Results"][(int)$PreviousQuestion["ID"]] = $_POST["input-slider"];
						$_SESSION[$SessionCurPtr] = $NextQuestion;
						array_push($_SESSION[$SessionPrvPtr], $NextQuestion);
						break;
					case "Radio":
						$RadioCheck = $_POST["input-radio"];
						$RadioPaths = $PreviousQuestion["EntryTypeOptionsPathing"];
						$_SESSION[$SessionCurPtr] = $RadioPaths[$RadioCheck];
						array_push($_SESSION[$SessionPrvPtr], $RadioPaths[$RadioCheck]);
						$_SESSION["Results"][(int)$PreviousQuestion["ID"]] = $RadioCheck;
						break;
					default:
						$NextQuestion = $PreviousQuestion["NextQuestion"];
						if($NextQuestion == 0)
						{
							echo "<script>L.marker([".$_SESSION[$ResultsLatPtr].",".$_SESSION[$ResultsLngPtr]."]).addTo(map).bindPopup('".$_SESSION[$ResultsLocPtr]."').openPopup();</script>";
                            
                            $LocalityInformationStored  = file_get_contents('pins.json');
                            $LocalityInformationArray   = json_decode($LocalityInformationStored);
                            $LocalityInformationArray[] = [$_SESSION[$ResultsLatPtr], $_SESSION[$ResultsLngPtr], $_SESSION[$ResultsLocPtr]];
                            $LocalityInformationJson    = json_encode($LocalityInformationArray);
                            file_put_contents('pins.json', $LocalityInformationJson);
                            
							$data_results = file_get_contents('results.json');
							$tempArray = json_decode($data_results);
							$tempArray[] = $_SESSION["Results"];
							$jsonData = json_encode($tempArray);
							file_put_contents('results.json', $jsonData);
							unset($_SESSION["Results"]);
							$_SESSION["Results"] = array();
							$_SESSION["Results"]["uniq"] = uniqid();
							$_SESSION["Results"]["date"] = date("Y-m-d H:i:s");
						}
						$_SESSION[$SessionCurPtr] = $NextQuestion;
						array_push($_SESSION[$SessionPrvPtr], $NextQuestion);
						break;
				}
			}
		}
	}
	else
	{
		$_SESSION[$SessionPrvPtr] = array();
		$_SESSION["Results"] = array();
		$_SESSION["Results"]["uniq"] = uniqid();
		$_SESSION["Results"]["date"] = date("Y-m-d H:i:s");
		$_SESSION[$SessionCurPtr] = 0;
	}


		if($_SESSION[$SessionCurPtr] == 0) {
			echo '<div class="smallContainer">';
		} else {
			echo '<div class="container">';
		}
		echo '<div id="questions">';


	$CurrentSurvey = $JsonFileParse[$_SESSION[$SessionCurPtr]];

	if($_SESSION[$SessionCurPtr] == 0) {
		echo "<h1 class='question-title-small'>".$CurrentSurvey["QuestionTitle"] ."</h1>";
	} else {
		echo "<h1 class='question-title'>".$CurrentSurvey["QuestionTitle"] ."</h1>";
	}

	echo "<hr>";

	if($CurrentSurvey["QuestionType"] == "None")
	{
		echo "<h1 class='tipLarge'>".$CurrentSurvey["QuestionHelpText"] ."</h1>";
	}
	else
	{
		echo "<h1 class='tip'>".$CurrentSurvey["QuestionHelpText"] ."</h1>";
	}



	if($CurrentSurvey["QuestionType"] == "ZipCode")
	{
		if($CurrentSurvey["Optional"])
		{
			echo "<input type='text' name='input-zipcode'>";
		}
		else
		{
			echo "<input type='text' name='input-zipcode' required>";
		}
	}
	else if($CurrentSurvey["QuestionType"] == "Paragraph")
	{
		if($CurrentSurvey["Optional"])
		{
			echo "<textarea rows='1' id='input-paragraph' name='input-paragraph'></textarea>";
		}
		else
		{
			echo "<textarea rows='1' id='input-paragraph' name='input-paragraph' required></textarea>";
		}
	}
	else if($CurrentSurvey["QuestionType"] == "Radio")
	{
		for ($i = 0; $i < sizeof($CurrentSurvey["QuestionTypeOptions"]); $i++)
		{
			if($CurrentSurvey["Optional"])
			{
				echo "<input type='radio' name='input-radio' value='".$i."'/>";
			}
			else
			{
				echo "<input type='radio' name='input-radio' value='".$i."' required/>";
			}
			echo "<label for='input-radio'>".$CurrentSurvey["QuestionTypeOptions"][$i]."</label><br />";
		}
	}
	else if($CurrentSurvey["QuestionType"] == "Slider")
	{
		echo "<br />";
		echo "<input id='input-slider' name='input-slider' value='1' onchange='updatePartySize(this.value);' type='range' min='".$CurrentSurvey["QuestionTypeOptions"][0]."' max='".$CurrentSurvey["QuestionTypeOptions"][1]."' step='1' /><br />";
		echo "<h1 id='partySize'>1 Person</h1>";
	}

	echo '</div>';
	include_once("index_navigation.php");
	echo '</div>';
?>
