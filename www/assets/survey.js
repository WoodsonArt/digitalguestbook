var map;

function initKeyboard()
{
	$("#osk-container").remove();
	$('input:text, textarea').onScreenKeyboard({leftPosition: "18%", topPosition: "55%", rewireTab : true});
}

function updatePartySize(count) 
{
	document.getElementById('partySize').innerHTML = count + " People";
	if(count == 1) 
	{
		document.getElementById('partySize').innerHTML = count + " Person";
	}
}

function goBack()
{
	$.ajax({type: 'POST', url: 'index_question.php', data: {"back": "1"}, dataType: 'html', encode: true}).done(function(data) 
	{
		$("#survey_questions").html(data).promise().done(initKeyboard);
		updatePartySize(1);
	});
}

$(document).ready(function() 
{
	$("#survey_questions").load('index_question.php', initKeyboard);
	
	map =  L.map('map', {minZoom: 3, maxZoom: 7}).setView([44.9620, -89.6130], 3);
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: '&copy; 2016 Leigh Yawkey Woodson Art Museum'}).addTo(map);
	
	$("#survey_questions").on("submit", function(event) 
	{
		var formData = $("#survey_questions").serialize();
		$.ajax({type: 'POST', url: 'index_question.php', data: formData, dataType: 'html', encode: true}).done(function(data) 
		{
			$("#survey_questions").html(data).promise().done(initKeyboard);
			updatePartySize(1);
        });
		event.preventDefault();
	});
});