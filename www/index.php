<!doctype html>
<html lang="en">
	<head>
		<title>Woodson Art Museum</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<script src="https://code.jquery.com/jquery-2.2.0.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="assets/leaflet.js"></script>
		<script src="assets/jquery.onScreenKeyboard.js"></script>
		<script src="assets/survey.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/onScreenKeyboard.css" />
		<link rel="stylesheet" type="text/css" href="assets/animate.css" />
		<link rel="stylesheet" type="text/css" href="assets/leaflet.css" />
		<link rel="stylesheet" type="text/css" href="assets/survey.css" />
	</head>
	<body>
		<div id="map"></div>
		<form id="survey_questions" action="index_question.php" method="POST">
		</form>
        <button type="button" onClick="location.reload(true);" class="btn btn-xl btn-default startover">Refresh</button>
        <script type="text/javascript">
            $(document).ready(function() {
                <?php include_once("index_pins.php"); ?>
            });
        </script>
	</body>
</html>
