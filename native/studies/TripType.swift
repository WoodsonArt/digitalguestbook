import Foundation

enum TripType : CustomStringConvertible
{
    case NoResponse
    case DayTrip
    case OvernightStay
    case Local
    
    var description : String
    {
        switch self
        {
            case .NoResponse: return "NoResponse"
            case .DayTrip: return "DayTrip"
            case .OvernightStay: return "OvernightStay"
            case .Local : return "Local"
        }
    }
}