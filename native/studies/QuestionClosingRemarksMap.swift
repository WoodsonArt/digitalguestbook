import Foundation
import UIKit
import MapKit

public class QuestionClosingRemarksMap:Question
{
    public override func onInitQuestion()
    {
        self.parentViewController.removeBackButton();
        self.parentViewController.addRestartButton();
        self.parentViewController.ibBoxView.hidden = true;
        self.parentViewController.ibQuestionTitleLabel.hidden = true;
        self.parentViewController.ibQuestionLabel.hidden = true;
        self.parentViewController.ibMainButton.hidden = true;
        self.parentViewController.ibTextBox.hidden = true;
        self.parentViewController.ibTextBoxLabel.hidden = true;
        self.parentViewController.ibSegmentedControl.hidden = true;
        self.parentViewController.zoomToUserEnteredLocation(self.parentViewController.preCollectedZipCode);
        self.parentViewController.ibSupplementaryLabel.hidden = true;
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(10 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue())
        {
            if let currentQuestion = self.parentViewController.currentQuestion as? QuestionClosingRemarksMap
            {
                self.parentViewController.startButtonClicked();
            }
        }
    }

    public override func onAnswer() -> Question?
    {
        return QuestionIntroduction(parentQuestion: self);
    }
}