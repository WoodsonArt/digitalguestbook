import Foundation
import UIKit

public class QuestionMoreAboutTrip:Question
{
    public override func onInitQuestion()
    {
        self.parentViewController.addBackButton();
        self.parentViewController.removeRestartButton();
        self.parentViewController.ibQuestionTitleLabel.numberOfLines = 1;
        self.parentViewController.ibQuestionTitleLabel.text = "Tell us more about your trip.";
        self.parentViewController.ibMainButton.setTitle("Next", forState: .Normal);
        self.parentViewController.ibTextBoxLabel.text = "";
        self.parentViewController.ibSegmentedControl.selectedSegmentIndex = -1;
        self.parentViewController.ibSegmentedControl.removeAllSegments();
        self.parentViewController.ibSegmentedControl.insertSegmentWithTitle("Day Trip", atIndex: 0, animated: true);
        self.parentViewController.ibSegmentedControl.insertSegmentWithTitle("Overnight Stay", atIndex: 1, animated: true);
        self.parentViewController.ibBoxView.hidden = false;
        self.parentViewController.ibQuestionTitleLabel.hidden = false;
        self.parentViewController.ibQuestionLabel.hidden = true;
        self.parentViewController.ibMainButton.hidden = false;
        self.parentViewController.ibTextBox.hidden = true;
        self.parentViewController.ibTextBoxLabel.hidden = true;
        self.parentViewController.ibSegmentedControl.hidden = false;
        self.parentViewController.ibSupplementaryLabel.hidden = true;
    }
    
    public override func onAnswer() -> Question?
    {
        if(self.parentViewController.ibSegmentedControl.selectedSegmentIndex > -1)
        {
            if(self.parentViewController.ibSegmentedControl.selectedSegmentIndex == 0)
            {
                self.parentViewController.collectedTripType = TripType.DayTrip;
            }
            else if(self.parentViewController.ibSegmentedControl.selectedSegmentIndex == 1)
            {
                self.parentViewController.collectedTripType = TripType.OvernightStay;
            }
            return QuestionMoreAboutMotivation(parentQuestion: self);
        }
        else
        {
            self.parentViewController.createAlertDialog("Please select a response.");
            return self;
        }
    }
}