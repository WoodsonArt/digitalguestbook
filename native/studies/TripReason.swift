import Foundation

public enum TripReason
{
    case NoResponse;
    case CurrentExhibition;
    case ProgramOrEvent;
    case Other;
    
    var description : String
    {
        switch self
        {
            case .NoResponse: return "NoResponse"
            case .CurrentExhibition: return "CurrentExhibition"
            case .ProgramOrEvent: return "ProgramOrEvent"
            case .Other : return "Other"
        }
    }
}