import Foundation
import UIKit

public class QuestionClosingRemarks:Question
{
    public override func onInitQuestion()
    {
        self.parentViewController.addBackButton();
        self.parentViewController.removeRestartButton();
        self.parentViewController.ibQuestionTitleLabel.numberOfLines = 2;
        self.parentViewController.ibQuestionTitleLabel.text = "Thanks for sharing!";
        self.parentViewController.ibMainButton.setTitle("Finish", forState: .Normal);
        self.parentViewController.ibQuestionLabel.text = "Return often and invite others to experience what's new on view at the always-admission-free Woodson Art Museum.";
        self.parentViewController.ibBoxView.hidden = false;
        self.parentViewController.ibQuestionTitleLabel.hidden = false;
        self.parentViewController.ibQuestionLabel.hidden = false;
        self.parentViewController.ibMainButton.hidden = false;
        self.parentViewController.ibTextBox.hidden = true;
        self.parentViewController.ibTextBoxLabel.hidden = true;
        self.parentViewController.ibSegmentedControl.hidden = true;
        self.parentViewController.ibSupplementaryLabel.hidden = true;
    }
    
    public override func onAnswer() -> Question?
    {
        return QuestionClosingRemarksMap(parentQuestion: self);
    }
}