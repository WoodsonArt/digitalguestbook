import Foundation
import UIKit

public class QuestionMoreAboutMotivationOther:Question
{
    public override func onInitQuestion()
    {
        self.parentViewController.addBackButton();
        self.parentViewController.removeRestartButton();
        
        self.parentViewController.ibQuestionTitleLabel.numberOfLines = 1;
        self.parentViewController.ibQuestionTitleLabel.text = "What brought you to the museum?";
        self.parentViewController.ibMainButton.setTitle("Next", forState: .Normal);
        self.parentViewController.ibTextBoxLabel.text = "Other";
        self.parentViewController.ibTextBox.keyboardType = UIKeyboardType.Default;
        self.parentViewController.ibTextBox.becomeFirstResponder();
        self.parentViewController.ibBoxView.hidden = false;
        self.parentViewController.ibQuestionTitleLabel.hidden = false;
        self.parentViewController.ibQuestionLabel.hidden = true;
        self.parentViewController.ibMainButton.hidden = false;
        self.parentViewController.ibTextBox.hidden = false;
        self.parentViewController.ibTextBoxLabel.hidden = false;
        self.parentViewController.ibSegmentedControl.hidden = true;
        self.parentViewController.ibSupplementaryLabel.hidden = true;
    }
    
    public override func onAnswer() -> Question?
    {
        if(self.parentViewController.ibTextBox.text!.characters.count > 0 && self.parentViewController.ibTextBox.text != nil && !self.parentViewController.ibTextBox.text!.isEmpty)
        {
            self.parentViewController.collectedTripReasonStatement = self.parentViewController.ibTextBox.text!;
            self.parentViewController.collectedTripReason = TripReason.Other;
            self.parentViewController.ibTextBox.endEditing(true);
            self.parentViewController.ibTextBox.text = "";
            return QuestionMoreAboutPartySize(parentQuestion: self);
        }
        else
        {
            self.parentViewController.createAlertDialog("Please enter a response.");
            return self;
        }
    }
}