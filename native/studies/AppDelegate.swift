import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window:UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        UINavigationBar.appearance().barTintColor = UIColor(red: 147.0/255.0, green: 140.0/255.0, blue: 126.0/255.0, alpha:1.0);
        UINavigationBar.appearance().tintColor = UIColor.whiteColor();
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Avenir", size: 24)!];
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Avenir", size: 24)!], forState: UIControlState.Normal);
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Avenir", size: 24)!], forState: UIControlState.Normal);
        return true
    }

    func applicationWillResignActive(application: UIApplication)
    {
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
    }

    func applicationWillTerminate(application: UIApplication)
    {
        self.saveContext()
    }

    lazy var applicationDocumentsDirectory: NSURL =
    {
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask);
        return urls[urls.count-1] ;
    }()

    lazy var managedObjectModel: NSManagedObjectModel =
    {
        let modelURL = NSBundle.mainBundle().URLForResource("studies", withExtension: "momd")!;
        return NSManagedObjectModel(contentsOfURL: modelURL)!;
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? =
    {
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel);
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("studies.sqlite");
        var error: NSError? = nil;
        var failureReason = "There was an error creating or loading the application's saved data.";
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil;
            var dict = [String: AnyObject]();
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data";
            dict[NSLocalizedFailureReasonErrorKey] = failureReason;
            dict[NSUnderlyingErrorKey] = error;
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict);
            NSLog("Unresolved error \(error), \(error!.userInfo)");
            abort();
        } catch {
            fatalError()
        }
        return coordinator;
    }()

    lazy var managedObjectContext: NSManagedObjectContext? =
    {
        let coordinator = self.persistentStoreCoordinator;
        if coordinator == nil
        {
            return nil;
        }
        var managedObjectContext = NSManagedObjectContext();
        managedObjectContext.persistentStoreCoordinator = coordinator;
        return managedObjectContext;
    }()

    func saveContext ()
    {
        if let moc = self.managedObjectContext
        {
            var error: NSError? = nil;
            if moc.hasChanges
            {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    NSLog("Unresolved error \(error), \(error!.userInfo)");
                    abort();
                }
            }
        }
    }
}