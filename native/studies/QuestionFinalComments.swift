import Foundation
import UIKit

public class QuestionFinalComments:Question
{
    public override func onInitQuestion()
    {
        self.parentViewController.addBackButton();
        self.parentViewController.removeRestartButton();
        self.parentViewController.ibQuestionTitleLabel.numberOfLines = 2;
        self.parentViewController.ibQuestionTitleLabel.text = "What about the Woodson Art Museum and/or exhibitions and\nprograms inspire you? (Optional)";
        self.parentViewController.ibMainButton.setTitle("Next", forState: .Normal);
        self.parentViewController.ibTextBoxLabel.text = "Response";
        self.parentViewController.ibTextBox.keyboardType = UIKeyboardType.Default;
        self.parentViewController.ibTextBox.becomeFirstResponder();
        self.parentViewController.ibBoxView.hidden = false;
        self.parentViewController.ibQuestionTitleLabel.hidden = false;
        self.parentViewController.ibQuestionLabel.hidden = true;
        self.parentViewController.ibMainButton.hidden = false;
        self.parentViewController.ibTextBox.hidden = false;
        self.parentViewController.ibTextBoxLabel.hidden = false;
        self.parentViewController.ibSegmentedControl.hidden = true;
        self.parentViewController.ibSupplementaryLabel.hidden = true;
    }
    
    public override func onAnswer() -> Question?
    {
        self.parentViewController.collectedThoughts = self.parentViewController.ibTextBox.text!;
        self.parentViewController.ibTextBox.text = "";
        self.parentViewController.ibTextBox.endEditing(true);
        return QuestionClosingRemarks(parentQuestion: self);
    }
}