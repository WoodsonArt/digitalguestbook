import Foundation
import UIKit

public class QuestionIntroductionZipCode:Question
{
    let zipCodesDefinedAsLocal = ["54401", "54402", "54403", "54476", "54474", "54455", "54452"];
    
    public override func onInitQuestion()
    {
        self.parentViewController.addBackButton();
        self.parentViewController.removeRestartButton();
        self.parentViewController.ibQuestionTitleLabel.numberOfLines = 1;
        self.parentViewController.ibQuestionTitleLabel.text = "Where do you live?";
        self.parentViewController.ibMainButton.setTitle("Next", forState: .Normal);
        self.parentViewController.ibTextBoxLabel.text = "Zip Code";
        self.parentViewController.ibTextBoxLabel.numberOfLines = 2;
        self.parentViewController.ibTextBox.keyboardType = UIKeyboardType.NumberPad;
        self.parentViewController.ibTextBox.becomeFirstResponder();
        self.parentViewController.ibBoxView.hidden = false;
        self.parentViewController.ibQuestionTitleLabel.hidden = false;
        self.parentViewController.ibQuestionLabel.hidden = true;
        self.parentViewController.ibMainButton.hidden = false;
        self.parentViewController.ibTextBox.hidden = false;
        self.parentViewController.ibTextBoxLabel.hidden = false;
        self.parentViewController.ibSegmentedControl.hidden = true;
        self.parentViewController.ibSupplementaryLabel.hidden = false;
    }
    
    public override func onAnswer() -> Question?
    {
        if(self.parentViewController.ibTextBox.text!.characters.count > 0 && self.parentViewController.ibTextBox.text != nil && !self.parentViewController.ibTextBox.text!.isEmpty)
        {
            self.parentViewController.preCollectedZipCode = self.parentViewController.ibTextBox.text!;
            self.parentViewController.ibTextBox.text = "";
            self.parentViewController.ibTextBox.endEditing(true);
            if zipCodesDefinedAsLocal.contains(self.parentViewController.preCollectedZipCode)
            {
                return QuestionMoreAboutMotivation(parentQuestion: self);
            }
            else
            {
                return QuestionMoreAboutTrip(parentQuestion: self);
            }
        }
        else
        {
            self.parentViewController.createAlertDialog("Please enter your zip code.");
            return self;
        }
    }
}