import MapKit
import UIKit
import CoreData

public class ViewController: UIViewController
{
    @IBOutlet weak var ibMapView: MKMapView!;
    @IBOutlet weak var ibBoxView: ILTranslucentView!;
    @IBOutlet weak var ibQuestionTitleLabel: UILabel!;
    @IBOutlet weak var ibQuestionLabel: UILabel!;
    @IBOutlet weak var ibTextBoxLabel: UILabel!;
    @IBOutlet weak var ibSupplementaryLabel: UILabel!;
    @IBOutlet weak var ibMainButton: UIButton!;
    @IBOutlet weak var ibTextBox: UITextField!;
    @IBOutlet weak var ibSegmentedControl: UISegmentedControl!;
    let museumCoordinates:CLLocationCoordinate2D = CLLocationCoordinate2DMake(44.9620, -89.6130);
    let defaultThemeColor = UIColor(red: 147.0/255.0, green: 140.0/255.0, blue: 126.0/255.0, alpha: 0.3);
    var currentQuestion:Question! = nil;
    var museumRegion:MKCoordinateRegion = MKCoordinateRegion();
    var preCollectedZipCode:String = "N/A";
    var collectedZipCode:String = "N/A";
    var collectedTripType:TripType = TripType.NoResponse;
    var collectedTripReason:TripReason = TripReason.NoResponse;
    var collectedTripReasonStatement:String = "N/A";
    var collectedTripVisitorCount:Int = 0;
    var collectedThoughts:String = "N/A"
    var zipCodeVisitorCounts = Dictionary<String, Int!>()
    
    override public func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);
        self.navigationController?.navigationBar.translucent = true;
        self.edgesForExtendedLayout = UIRectEdge.All;
        self.navigationController?.navigationBar.opaque = false;
        zoomToMuseumLocation();
    }
    
    override public func viewDidLoad()
    {
        super.viewDidLoad();
        ibBoxView.translucentAlpha = 0.75;
        ibBoxView.translucentStyle = UIBarStyle.Default;
        ibBoxView.translucentTintColor = defaultThemeColor;
        ibBoxView.backgroundColor = UIColor.clearColor();
        ibMainButton.layer.borderWidth = 1;
        ibMainButton.layer.cornerRadius = 2;
        ibMainButton.layer.borderColor = UIColor.whiteColor().CGColor;
        ibQuestionTitleLabel.textAlignment = NSTextAlignment.Center;
        self.currentQuestion = QuestionIntroduction(ibViewController: self)
        self.currentQuestion.onInitQuestion();
        load()
    }
    
    override public func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
    }

    @IBAction func nextButtonClicked(sender: UIButton)
    {
        self.currentQuestion = self.currentQuestion.onAnswer()!;
        self.currentQuestion.onInitQuestion();
    }
    
    @IBAction func valueChangedSegmented(sender: UISegmentedControl)
    {
        self.currentQuestion.onSegmentedControlSelectionChanged();
    }
    
    public func zoomToUserEnteredLocation(userLocation:String)
    {

        
        
        
        let geocoder:CLGeocoder = CLGeocoder();
        geocoder.geocodeAddressString(userLocation, completionHandler: {(placemarks, error) -> Void in
        if((error) != nil)
        {
            self.createAlertDialog("We were unable to locate your zip code. Please try again!");
        }
        else
        {
            let placemark = placemarks![0]
            let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate;
            let pointAnnotation:MKPointAnnotation = MKPointAnnotation();
            var region:MKCoordinateRegion = MKCoordinateRegion();
            pointAnnotation.coordinate = coordinates;
            
            
            
            
            
            
            
            
            self.collectedZipCode = placemark.postalCode!;
            
            
            print(self.collectedZipCode);
            
           
            
            var visitors = 0;
            
            if let val = self.zipCodeVisitorCounts[self.collectedZipCode]
            {
                self.zipCodeVisitorCounts.updateValue(val + self.collectedTripVisitorCount, forKey: self.collectedZipCode)
                visitors = val + self.collectedTripVisitorCount;
            }
            else
            {
                self.zipCodeVisitorCounts.updateValue(self.collectedTripVisitorCount, forKey: self.collectedZipCode)
                visitors = self.collectedTripVisitorCount
            }
            
            

            if let locality = placemark.locality
            {
                if(visitors > 1)
                {
                    pointAnnotation.title = String(visitors) + " visitors from " + locality
                }
                else
                {
                    pointAnnotation.title = String(visitors) + " visitor from " +  locality
                }
            }
            else
            {
                if(visitors > 1)
                {
                    pointAnnotation.title = String(visitors) + " visitors from " + self.collectedZipCode
                }
                else
                {
                    pointAnnotation.title = String(visitors) + " visitor from " +  self.collectedZipCode
                }
            }
            


            


            
            
            
           
            
            region.center.latitude = coordinates.latitude;
            region.center.longitude = coordinates.longitude;
            region.span.latitudeDelta = 5;
            region.span.longitudeDelta = 5;
            self.ibMapView.addAnnotation(pointAnnotation);
            self.ibMapView.centerCoordinate = coordinates;
            self.ibMapView.selectAnnotation(pointAnnotation, animated: false);
            self.ibMapView.setRegion(region, animated: true);
            
            
             self.save()
            
            
        }});
    }
    
    public func zoomToMuseumLocation()
    {
        museumRegion.center.latitude = museumCoordinates.latitude;
        museumRegion.center.longitude = museumCoordinates.longitude;
        museumRegion.span.latitudeDelta = 100;
        museumRegion.span.longitudeDelta = 100;
        self.ibMapView.setCenterCoordinate(museumCoordinates, animated: true);
        self.ibMapView.setRegion(museumRegion, animated: true);
    }
    
    public func addBackButton()
    {
        let backButton:UIBarButtonItem = UIBarButtonItem(title: "Go Back", style: UIBarButtonItemStyle.Plain, target: self, action: "backButtonClicked");
        backButton.tintColor = UIColor.whiteColor();
        self.navigationItem.leftBarButtonItem = backButton;
    }
    
    public func removeBackButton()
    {
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    public func backButtonClicked()
    {
        self.ibTextBox.endEditing(true);
        self.currentQuestion = self.currentQuestion.parentQuestion;
        self.currentQuestion.onInitQuestion();
    }
    
    public func addRestartButton()
    {
        let backButton:UIBarButtonItem = UIBarButtonItem(title: "Start", style: UIBarButtonItemStyle.Plain, target: self, action: "startButtonClicked");
        backButton.tintColor = UIColor.whiteColor();
        self.navigationItem.rightBarButtonItem = backButton;
    }
    
    public func removeRestartButton()
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    public func startButtonClicked()
    {
        self.ibTextBox.endEditing(true);
        zoomToMuseumLocation();
        
        self.currentQuestion = QuestionIntroduction(ibViewController: self);
        self.currentQuestion.onInitQuestion();
        
        self.preCollectedZipCode = "N/A";
        self.collectedZipCode = "N/A";
        self.collectedTripType = TripType.NoResponse;
        self.collectedTripReason = TripReason.NoResponse;
        self.collectedTripReasonStatement = "N/A";
        self.collectedTripVisitorCount = 0;
        self.collectedThoughts = "N/A"
        for annotation in ibMapView.annotations 
        {
            if let annotationView = ibMapView.viewForAnnotation(annotation) as? MKPinAnnotationView
            {
                annotationView.pinColor = MKPinAnnotationColor.Purple;
            }
            ibMapView.deselectAnnotation(annotation, animated: true);
        }
    }
    
    public func save()
    {
    }
    
    public func load()
    {
    }
    
    public func createAlertDialog(message:String)
    {
        let alert = UIAlertView();
        alert.title = "Alert";
        alert.message = message;
        alert.addButtonWithTitle("Ok");
        alert.show();
    }
}
