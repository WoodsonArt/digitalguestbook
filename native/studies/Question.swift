import Foundation
import UIKit
import MapKit

public class Question
{
    public var parentQuestion:Question! = nil;
    public var parentViewController:ViewController;
    
    public init(ibViewController:ViewController)
    {
        self.parentViewController = ibViewController;
    }
    
    public init(parentQuestion:Question?)
    {
        self.parentQuestion = parentQuestion;
        self.parentViewController = self.parentQuestion.parentViewController;
    }
    
    public func onInitQuestion()
    {
        preconditionFailure("This method must be overridden");
    }
    
    public func onAnswer() -> Question?
    {
        preconditionFailure("This method must be overridden");
    }
    
    public func onSegmentedControlSelectionChanged()
    {
    }
}