import Foundation
import UIKit

public class QuestionMoreAboutMotivation:Question
{
    public override func onInitQuestion()
    {
        self.parentViewController.addBackButton();
        self.parentViewController.removeRestartButton();
        self.parentViewController.ibQuestionTitleLabel.numberOfLines = 1;
        self.parentViewController.ibQuestionTitleLabel.text = "What brought you to the museum?";
        self.parentViewController.ibMainButton.setTitle("Next", forState: .Normal);
        self.parentViewController.ibTextBoxLabel.text = "";
        self.parentViewController.ibSegmentedControl.selectedSegmentIndex = -1;
        self.parentViewController.ibSegmentedControl.removeAllSegments();
        self.parentViewController.ibSegmentedControl.insertSegmentWithTitle("Current Exhibition", atIndex: 0, animated: true);
        self.parentViewController.ibSegmentedControl.insertSegmentWithTitle("Program or Event", atIndex: 1, animated: true);
        self.parentViewController.ibSegmentedControl.insertSegmentWithTitle("Other", atIndex: 2, animated: true);
        self.parentViewController.ibBoxView.hidden = false;
        self.parentViewController.ibQuestionTitleLabel.hidden = false;
        self.parentViewController.ibQuestionLabel.hidden = true;
        self.parentViewController.ibMainButton.hidden = false;
        self.parentViewController.ibTextBox.hidden = true;
        self.parentViewController.ibTextBoxLabel.hidden = true;
        self.parentViewController.ibSegmentedControl.hidden = false;
        self.parentViewController.ibSupplementaryLabel.hidden = true;
    }
    
    public override func onAnswer() -> Question?
    {
        if(self.parentViewController.ibSegmentedControl.selectedSegmentIndex > -1)
        {
            if(self.parentViewController.ibSegmentedControl.selectedSegmentIndex == 0)
            {
                self.parentViewController.collectedTripReason = TripReason.CurrentExhibition;
            }
            else if(self.parentViewController.ibSegmentedControl.selectedSegmentIndex == 1)
            {
                self.parentViewController.collectedTripReason = TripReason.ProgramOrEvent;
            }
            else if(self.parentViewController.ibSegmentedControl.selectedSegmentIndex == 2)
            {
                self.parentViewController.collectedTripReason = TripReason.Other;
            }
            return QuestionMoreAboutPartySize(parentQuestion: self);
        }
        else
        {
            self.parentViewController.createAlertDialog("Please select a response.");
            return self;
        }
    }
    
    public override func onSegmentedControlSelectionChanged()
    {
        if(self.parentViewController.ibSegmentedControl.selectedSegmentIndex == 2)
        {
            self.parentViewController.currentQuestion = QuestionMoreAboutMotivationOther(parentQuestion: self);
            self.parentViewController.currentQuestion.onInitQuestion();
        }
    }
}