import Foundation
import UIKit

public class QuestionIntroduction:Question
{
    public override func onInitQuestion()
    {
        self.parentViewController.removeBackButton();
        self.parentViewController.removeRestartButton();
        self.parentViewController.ibQuestionTitleLabel.numberOfLines = 2;
        self.parentViewController.ibQuestionTitleLabel.text = "Make Your Mark\nWoodson Art Museum Visitors";
        self.parentViewController.ibMainButton.setTitle("Start", forState: .Normal);
        self.parentViewController.ibBoxView.hidden = false;
        self.parentViewController.ibQuestionTitleLabel.hidden = false;
        self.parentViewController.ibQuestionLabel.hidden = true;
        self.parentViewController.ibMainButton.hidden = false;
        self.parentViewController.ibTextBox.hidden = true;
        self.parentViewController.ibTextBoxLabel.hidden = true;
        self.parentViewController.ibSegmentedControl.hidden = true;
        self.parentViewController.ibSupplementaryLabel.hidden = true;
    }
    
    public override func onAnswer() -> Question?
    {
        return QuestionIntroductionZipCode(parentQuestion: self);
    }
}